jQuery('#top-menu').ready(function(){
    if( !jQuery('body.page').hasClass('home') && jQuery('body').width() > 1000 ){
        adaptTopMenu();
    }
    jQuery('#top-menu ul.sub-menu').parent().addClass('parent');

    if (jQuery.cookie('font-size') && jQuery.cookie('font-size')=='big'){
        jQuery('#trigger-font-size').attr('title','Decrease font size');
    }
    else {
        jQuery('#trigger-font-size').attr('title','Increase font size');
    }
    jQuery('#top-menu .parent > a').live('click',function(e){
        e.preventDefault();
        var topMenu = jQuery('#top-menu');
        jQuery('#top-menu .menu > li > a').removeClass('dark');
        jQuery('#header .sub-menu').hide();
        jQuery('#header .orange').removeClass('orange');
        jQuery('#top-menu .desktop').removeClass('desktop');
        jQuery('#header > .container > .sub-menu').remove();
        
        jQuery('#top-menu .menu > li > a').addClass('dark');
        if( jQuery(window).width() < 1300 || !jQuery('body.page').hasClass('home') || (jQuery(window).width() < 1500 && jQuery('body').hasClass('big-font'))){
            if (!jQuery('body.page').hasClass('home') && jQuery(window).width() > 1100 ){
                jQuery(this).parent().addClass('orange');
                var _secondUl = jQuery(this).parent().find('.sub-menu').first().clone();
                _secondUl.insertAfter(jQuery('#top-menu')).addClass('doubled');
                var _firstUl =_secondUl.clone();
                _firstUl.insertAfter(jQuery('#top-menu'));
                var _elementsDiv2 = Math.ceil(jQuery('li',_firstUl).length/2);
                jQuery('li:gt('+(_elementsDiv2-1)+')', _firstUl).hide();
                jQuery('li:lt('+_elementsDiv2+')', _secondUl).remove();
                _secondUl.show();
                _firstUl.show();
            }
            else {
                jQuery(this).parent().addClass('orange').find('.sub-menu').first().clone().insertAfter(jQuery('#top-menu')).show();
            }

        }
        else if ( jQuery('body.page').hasClass('home') && jQuery(window).width() > 1024 ){
            jQuery(this).parent().addClass('orange').find('.sub-menu').first().show().addClass('desktop');
            var submenu = jQuery(this).parent().find('.sub-menu');
            var submenuLeft=190;
            var hFromThisToEnd = topMenu.height()+topMenu.offset().top-jQuery(this).offset().top;
            if (jQuery('body.page').hasClass('big-font')) {
                submenuLeft=220;
            }
            if (hFromThisToEnd < submenu.height()){
                submenu.css('top',hFromThisToEnd-submenu.height()-5).css('left',submenuLeft+(submenu.height()-hFromThisToEnd)*1.05);
            }
            else {
                submenu.css('left',submenuLeft);
            }


        }
    })

    jQuery('#trigger-font-size').bind('click',function(){

        jQuery('body').toggleClass('big-font');
        if( jQuery('body').hasClass('big-font') ){
            jQuery('#trigger-font-size').attr('title','Decrease font size');
            jQuery.cookie('font-size','big',{expires:7,path: '/'});
        }
        else {
            jQuery('#trigger-font-size').attr('title','Increase font size');
            jQuery.cookie('font-size','small',{expires:7,path: '/'});
        }
        
        if( jQuery(window).width() > 1000 && !jQuery('body.page').hasClass('home') ){
            jQuery('#top-menu a:contains("Website Search & Tools")').html('Website Search <br /> & Tools');
        }
    })
    jQuery('#s').focus(function(){
        jQuery(this).attr('placeholder','');
    })
    jQuery('#s').blur(function(){
        jQuery(this).attr('placeholder','Search...');
    })
})

function adaptTopMenu(){
    //check container width
    var _countItems = jQuery('#menu-main-menu > li').length;
    var _rest = _countItems % 3;
    var _countItemsInCol = 0;
    
    if(_rest==1){
        _countItemsInCol = (_countItems-1)/3;
        var it2 = jQuery('#menu-main-menu > li').slice(_countItemsInCol+1 , 2*_countItemsInCol+1);
        var it3 = jQuery('#menu-main-menu > li').slice(2*_countItemsInCol+1);
    }
        
    else if(_rest==2){
        _countItemsInCol = (_countItems-2)/3;
        var it2 = jQuery('#menu-main-menu > li').slice(_countItemsInCol+1 , 2*_countItemsInCol+2);
        var it3 = jQuery('#menu-main-menu > li').slice(2*_countItemsInCol+2);
    }
    else {
        _countItemsInCol = _countItems/3;
        var it2 = jQuery('#menu-main-menu > li').slice(_countItemsInCol , 2*_countItemsInCol);
        var it3 = jQuery('#menu-main-menu > li').slice(2*_countItemsInCol);
    }
    
    jQuery('#menu-main-menu').after('<ul class="menu it3"></ul>').after('<ul class="menu it2"></ul>');
    it2.appendTo('#top-menu .menu.it2');
    it3.appendTo('#top-menu .menu.it3');
    jQuery('#top-menu a:contains("Website Search & Tools")').html('Website Search <br /> & Tools');
}
function adaptHomeSubMenu(){
        jQuery('#top-menu .menu .sub-menu.desktop').hide().removeClass('desktop').clone().insertAfter(jQuery('#top-menu')).show();
}
function hideDoubledSubMenu(){
    jQuery('.container > .sub-menu:eq(1)').hide();
    jQuery('.container > .sub-menu:eq(0) li').show();
}

jQuery(window).resize(function(){
    if( !jQuery('body.page').hasClass('home') && jQuery('body').width() > 1100 && jQuery('#top-menu ul.menu').length < 2 ){
        adaptTopMenu();
    }
    else if ( !jQuery('body.page').hasClass('home') && jQuery('body').width() < 1100 && jQuery('#top-menu ul.menu').length > 2 ){
        var _set = jQuery('#top-menu ul.menu > li');
        jQuery('#top-menu ul.menu.it2, #top-menu ul.menu.it3').remove();
        jQuery('#top-menu ul#menu-main-menu').empty().append(_set);
        hideDoubledSubMenu();
    }
    if ( jQuery('body.page').hasClass('home') &&
            (jQuery(window).width() < 1300 || (jQuery(window).width() < 1500 && jQuery('body').hasClass('big-font')))
            && jQuery('#top-menu .menu .sub-menu').hasClass('desktop')
            ){
        adaptHomeSubMenu();
    }

})

jQuery('#login-box').ready(function(){
    // Custom checkbox
    jQuery('#login-box .forgetmenot-label input').css('display','none').after('<i></i>');
    jQuery('#login-box .forgetmenot-label').css('paddingLeft','22px');
})


//jQuery('#menu-bottom-nenu').ready(function(){
//    jQuery('#menu-bottom-nenu a').on('click', function(e){
//        e.preventDefault();
//        var _targ = jQuery(this).parent();
//        jQuery('#menu-bottom-nenu li').removeClass('current-menu-item');
//        _targ.addClass('current-menu-item');
//        var _index = jQuery('#menu-bottom-nenu li').index(_targ);
//        jQuery('#main .tp-thumbcontainer .bullet').eq(_index).find('img').trigger('click');
//    })
//})

function menuItemActivate(o){
    var _indexActive = jQuery('.rev_slider > ul > li').index(o);
    var _targ = jQuery('#menu-bottom-nenu li');
    if( !_targ.eq(_indexActive).hasClass('current-menu-item') )
    {
        _targ.removeClass('current-menu-item');
        _targ.eq(_indexActive).addClass('current-menu-item');
    }
}

