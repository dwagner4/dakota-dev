			<footer role="contentinfo">
				<div id="inner-footer" class="clearfix">
                    <div class="container">
                        <?php dynamic_sidebar('footer1') ?>

                            <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
                                <a class="addthis_button_twitter"></a>
                                <a class="addthis_button_facebook"></a>
                                <a class="addthis_button_tumblr"></a>
                                <a class="addthis_button_pinterest_share"></a>
                                <a class="addthis_button_google_plusone_share"></a>
                                <a class="addthis_button_linkedin"></a>
                                <a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
                                <div class="rss"><?php  dynamic_sidebar( 'rss-link' ); ?></div>
                            </div>
                            <script type="text/javascript">var addthis_config = {"data_track_addressbar":false};</script>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5288cbdf3966d43e"></script>
                        
                        <!-- <div class="share42init"></div> -->
                        <!-- <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/share42/share42.js"></script> --> 
    				</div>
				</div> <!-- end #inner-footer -->
			 
			</footer> <!-- end footer -->
            <script>
             // load big font
             if (jQuery.cookie('font-size') && jQuery.cookie('font-size')=='big'){
                 jQuery('body').toggleClass('big-font');
             }
             </script>
		<!--[if lt IE 7 ]>
  			<script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
  			<script>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
		<![endif]-->
		
		<?php wp_footer(); // js scripts are inserted using this function ?>

	</body>

</html>