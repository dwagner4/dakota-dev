<?php
/*
Template Name: Page without header
*/
?>

<?php get_header(); ?>
			
			<div id="content" class="clearfix row-fluid">
			
				<div id="main" class="span8 clearfix" role="main">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					
					<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						
						<section class="post_content clearfix" itemprop="articleBody">
							<?php the_content(); ?>
					
						</section> <!-- end article section -->
						
						<footer>
			
							<?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","bonestheme") . ':</span> ', ', ', '</p>'); ?>
							
						</footer> <!-- end article footer -->
					
					</article> <!-- end article -->
					
					<?php comments_template('',true); ?>
					
					<?php endwhile; ?>		
					
					<?php else : ?>
					
					<article id="post-not-found">
					    <header>
					    	<h1><?php _e("Not Found", "bonestheme"); ?></h1>
					    </header>
					    <section class="post_content">
					    	<p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
					    </section>
					</article>
					
					<?php endif; ?>
                    
                    <div id="bottom-menu" role="menu">
        				
    					<?php if ( is_active_sidebar( 'sidebsr-for-bottommenu' ) ) : ?>
    						<?php dynamic_sidebar( 'sidebsr-for-bottommenu' ); ?>
    					<?php else : ?>
    						<!-- This content shows up if there are no widgets defined in the backend. -->
    						<div class="alert alert-message">
    							<p><?php _e("Please activate some Widgets","bonestheme"); ?>.</p>
    						</div>
    					<?php endif; ?>
    
    				</div>
                
				</div> <!-- end #main -->
    
				<?php get_sidebar(); // sidebar 1 ?>
    
			</div> <!-- end #content -->
            
        </div> <!-- end #container -->

<?php get_footer(); ?>