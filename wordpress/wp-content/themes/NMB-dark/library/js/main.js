jQuery('#top-menu').ready(function(){
    if( !jQuery('body.page').hasClass('home') && jQuery('body').width() > 1000 ){
        adaptTopMenu();
    }
    jQuery('#top-menu ul.sub-menu').parent().addClass('parent');
    jQuery('#top-menu .parent > a').bind('click',function(e){
        e.preventDefault();
        
        jQuery('#top-menu .menu > li > a').removeClass('dark');
        jQuery('#header .sub-menu').hide();
        jQuery('#header .orange').removeClass('orange');
        jQuery('#header > .container > .sub-menu').remove();
        
        jQuery('#top-menu .menu > li > a').addClass('dark');
        if( jQuery(window).width() < 1025 || !jQuery('body.page').hasClass('home') ){
        jQuery(this).parent().addClass('orange').find('.sub-menu').first().clone().insertAfter(jQuery('#top-menu')).show();
        }
        else if ( jQuery('body.page').hasClass('home') && jQuery(window).width() > 1024 ){
            jQuery(this).parent().addClass('orange').find('.sub-menu').first().show().addClass('desktop');
        }
    })
    
    jQuery('#trigger-font-size').attr('title','Increase font size');
    
    jQuery('#trigger-font-size').bind('click',function(){
        jQuery('body').toggleClass('big-font');
        if( jQuery('body').hasClass('big-font') ){ jQuery('#trigger-font-size').attr('title','Decrease font size') }
        else { jQuery('#trigger-font-size').attr('title','Increase font size') }
    })
    jQuery('#s').focus(function(){
        jQuery(this).attr('placeholder','');
    })
    jQuery('#s').blur(function(){
        jQuery(this).attr('placeholder','Search...');
    })
})

function adaptTopMenu(){
    var _countItems = jQuery('#menu-main-menu > li').length;
    var _rest = _countItems % 3;
    var _countItemsInCol = 0;
    
    if(_rest==1){
        _countItemsInCol = (_countItems-1)/3;
        var it2 = jQuery('#menu-main-menu > li').slice(_countItemsInCol+1 , 2*_countItemsInCol+1);
        var it3 = jQuery('#menu-main-menu > li').slice(2*_countItemsInCol+1);
    }
        
    else if(_rest==2){
        _countItemsInCol = (_countItems-2)/3;
        var it2 = jQuery('#menu-main-menu > li').slice(_countItemsInCol+1 , 2*_countItemsInCol+2);
        var it3 = jQuery('#menu-main-menu > li').slice(2*_countItemsInCol+2);
    }
    else {
        _countItemsInCol = _countItems/3;
        var it2 = jQuery('#menu-main-menu > li').slice(_countItemsInCol , 2*_countItemsInCol);
        var it3 = jQuery('#menu-main-menu > li').slice(2*_countItemsInCol);
    }
    
    jQuery('#menu-main-menu').after('<ul class="menu it3"></ul>').after('<ul class="menu it2"></ul>');
    it2.appendTo('#top-menu .menu.it2');
    it3.appendTo('#top-menu .menu.it3');
}

jQuery(window).resize(function(){
    if( !jQuery('body.page').hasClass('home') && jQuery('body').width() > 1000 && jQuery('#top-menu ul.menu').length < 2 ){
        adaptTopMenu();
    }
    else if ( !jQuery('body.page').hasClass('home') && jQuery('body').width() < 1000 && jQuery('#top-menu ul.menu').length > 2 ){
        var _set = jQuery('#top-menu ul.menu > li');
        jQuery('#top-menu ul.menu.it2, #top-menu ul.menu.it3').remove();
        jQuery('#top-menu ul#menu-main-menu').empty().append(_set);
    }
})

jQuery('#login-box').ready(function(){
    // Custom checkbox
    jQuery('#login-box .forgetmenot-label input').css('display','none').after('<i></i>');
    jQuery('#login-box .forgetmenot-label').css('paddingLeft','22px');
    
    // pop-up is refreshed after closing/openning
    jQuery('#login-box .close-btn').on('click',function(){
        jQuery('#login #form input[type="text"], #login #form input[type="password"]').val('');
        jQuery('#login #form input[type="checkbox"]').removeAttr("checked");
    })
})

