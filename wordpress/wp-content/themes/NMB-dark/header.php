<!doctype html>  

<!--[if IEMobile 7 ]> <html <?php language_attributes(); ?>class="no-js iem7"> <![endif]-->
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8"> <![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		
		<title><?php wp_title( '|', true, 'right' ); ?></title>
				
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
				
		<!-- media-queries.js (fallback) -->
		<!--[if lt IE 9]>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>			
		<![endif]-->

		<!-- html5.js -->
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
  		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

		<!-- wordpress head functions -->
		<?php wp_head(); ?>
		<!-- end of wordpress head -->

		<!-- theme options from options panel -->
		<?php get_wpbs_theme_options(); ?>

		<!-- typeahead plugin - if top nav search bar enabled -->
		<?php require_once('library/typeahead.php'); ?>
        
        <link type="text/css" href="<?php bloginfo('template_url'); ?>/selectbox.css" rel="stylesheet" media="screen" />
        <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/library/js/selectbox.js"></script> 
        <script type="text/javascript" src="/wp-content/plugins/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="/wp-content/plugins/js/main.js"></script>

        <link href='https://fonts.googleapis.com/css?family=Roboto:100' rel='stylesheet' type='text/css' />

        <link href='https://fonts.googleapis.com/css?family=Average+Sans' rel='stylesheet' type='text/css' />
        <link rel="shortcut icon" href="/wp-content/favicon.png" type="image/x-png" />
        <link href="/wp-content/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />

	</head>
	
	<body <?php body_class(); ?>>
    
		<header id="header" role="banner">
		
			<div id="inner-header" class="clearfix">
				
				<div class="navbar">
					<div class="navbar-inner">
						<div class="container-fluid nav-container">
							<nav role="navigation">
                            
							<!-- <?php if(of_get_option('search_bar', '1')) {?>
							<form class="navbar-search pull-right" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
								<input name="s" id="s" type="text" class="search-query" autocomplete="off" placeholder="<?php _e('Search','bonestheme'); ?>" data-provide="typeahead" data-items="4" data-source='<?php echo $typeahead_data; ?>'>
							</form>
							<?php } ?> -->

                            <div id="log-out" class="pull-right">
                                <span>
                                    <?php

                                    if(wp_get_current_user()->id != 0){
                                        echo '<a href="/profile">'.wp_get_current_user()->user_login.'</a> | ';
                                        add_modal_login_button( $login_text = 'Login', $logout_text = 'Logout', $logout_url = '', $show_admin = false);
                                    }else
                                        add_modal_login_button( $login_text = 'Login', $logout_text = 'Logout', $logout_url = '', $show_admin = false  == "Logout");
                                    ?>
                                </span>

                            </div>

                            <div id="switch-theme" class="pull-right">
                                <?php wp_theme_switcherLight(); ?>
                            </div>

                            <div id="trigger-font-size" class="pull-right">
                                <span><img src="/wp-content/themes/NMB-light/library/img/text-switch-light.png" alt=" "></span>
                            </div>
                            <div id="lang-select" class="pull-right">
<!--                                --><?php //echo qtrans_generateLanguageSelectCode('dropdown'); ?>
                                    <?php echo do_shortcode('[google-translator]'); ?>
                            </div>

							<form class="navbar-search pull-right" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
								<input name="s" id="s" type="text" class="search-query" autocomplete="off" placeholder="<?php _e('Search...','bonestheme'); ?>" data-provide="typeahead" data-items="4" data-source='<?php echo $typeahead_data; ?>'>
                                <input class="submit" type="submit" value=" " />
                            </form>

								<!-- <a class="brand" id="logo" title="<?php echo get_bloginfo('description'); ?>" href="<?php echo home_url(); ?>">
									<?php if(of_get_option('branding_logo','')!='') { ?>
										<img src="<?php echo of_get_option('branding_logo'); ?>" alt="<?php echo get_bloginfo('description'); ?>">
										<?php }
										if(of_get_option('site_name','1')) bloginfo('name'); ?></a> -->
								
								<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							        <span class="icon-bar"></span>
							        <span class="icon-bar"></span>
							        <span class="icon-bar"></span>
								</a>
								
								<div class="nav-collapse">
									<?php bones_main_nav(); // Adjust using Menus in Wordpress Admin ?>
								</div>
								
							</nav>
							
						</div> <!-- end .nav-container -->
					</div> <!-- end .navbar-inner -->
				</div> <!-- end .navbar -->
			
			</div> <!-- end #inner-header -->
            
            <div class="container">
                <a href="/" id="logo" title="The National Mediation Board"></a>
                <h2></h2>
    			<div id="top-menu" role="menu">
    				
					<?php if ( is_active_sidebar( 'sidebsr-for-topmenu' ) ) : ?>
						<?php dynamic_sidebar( 'sidebsr-for-topmenu' ); ?>
					<?php else : ?>
						<!-- This content shows up if there are no widgets defined in the backend. -->
						<div class="alert alert-message">
							<p><?php _e("Please activate some Widgets","bonestheme"); ?>.</p>
						</div>
					<?php endif; ?>

				</div>
                
            </div>
            
		</header> <!-- end header -->
		
		<div class="container-fluid">
        
<?php if (function_exists('dimox_breadcrumbs')) dimox_breadcrumbs(); ?> 
<?php /* <div id="breadcrumbs"><div class="breadcrumbs"><?php if(function_exists('bcn_display')) bcn_display(); ?></div></div> */ ?>
<?php /* <div id="breadcrumbs"><?php the_breadcrumb(); ?></div> */ ?>

