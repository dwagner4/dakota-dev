$(window).load(function () {
});
$(document).ready(function () {
	$("#navi").accordion({
		collapsible: true,
		autoHeight: false,
		active: false
	});
	$("#navi h3").click(function () {
		$(this).blur();
	});
	$("a").click(function () {
		$(this).blur();
	});
	
	$('body.about #navi').accordion({active: 0 });
	$('body.mda #navi').accordion({active: 1 });
	$('body.rep #navi').accordion({active: 2 });
	$('body.financial #navi').accordion({active: 3 });
	$('body.rgarbitration #navi').accordion({active: 4 });
	$('body.supporting #navi').accordion({active: 5 });
	
});